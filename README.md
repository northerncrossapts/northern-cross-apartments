Northern Cross is a beautiful community located in North Fort Worth on Beach Street just off of the newly constructed I-820 near the Fossil Creek Golf Club. Northern Cross offers well-designed floor plans with newly upgraded interiors. Enjoy a large variety of community and home amenities offered to give the peace and comfort you need in a luxury apartment home.

Address: 4200 Northern Cross Blvd, Haltom City, TX 76137, USA
Phone: 817-993-5327
